from pgmpy.models import BayesianModel
from pgmpy.estimators import MaximumLikelihoodEstimator, BayesianEstimator
from pgmpy.inference import VariableElimination
import pandas as pd

# Load the Alarm dataset
data = pd.read_csv('https://raw.githubusercontent.com/pgmpy/pgmpy/master/examples/Alarm.csv')

# Define the structure of the Bayesian Network
model = BayesianModel([('Burglary', 'Alarm'), ('Earthquake', 'Alarm'), ('Alarm', 'JohnCalls'), ('Alarm', 'MaryCalls')])

# Estimate the parameters of the model using Maximum Likelihood Estimation
model.fit(data, estimator=MaximumLikelihoodEstimator)

# Perform Bayesian parameter estimation
model.fit(data, estimator=BayesianEstimator, prior_type='BDeu')

# Perform inference on the model
infer = VariableElimination(model)

# Query the model for a specific variable
query_result = infer.query(variables=['JohnCalls'], evidence={'MaryCalls': 1, 'Burglary': 0})

# Print the query result
print(query_result['JohnCalls'])